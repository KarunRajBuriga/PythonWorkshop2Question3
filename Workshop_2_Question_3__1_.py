#!/usr/bin/env python
# coding: utf-8

# In[12]:



import pandas as pd 
import requests
import json

data = requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-01-01&endtime=2017-12-31&alertlevel=yellow")
data = json.loads(data.text)
a = data["features"]         
df = pd.DataFrame(a)  
b = df.properties 
c = pd.json_normalize(b)
c


# In[13]:


import pyodbc 
server = 'OTUSDPSQL'
database = 'B12022_Target_KBuriga'
username = 'B12022_KBuriga'
password = ''
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()


# In[14]:


cursor.execute('create table api(Place varchar(50), Type varchar(25), Magnitude numeric(3,1), Title varchar(50))')


# In[ ]:


for index,row in c.iterrows():
    cursor.execute("INSERT INTO dbo.api values(?,?,?,?)", row['place'],row['type'],row['cdi'],row['title'])


# In[ ]:


# vi)	Provide query/analysis to give biggest earthquake of 2017
cursor.execute('select top 1 * from dbo.apiData order by Magnitude desc')


# In[ ]:


cnxn.commit()


# In[7]:



cursor.close()


# In[ ]:



c.to_csv('Documents/api.csv')


# In[9]:


import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('Documents/api.csv')


# In[10]:


df.plot()


# In[15]:


plt.show()


# In[ ]:




